require('dotenv').config();

const Mirai = require('node-mirai-sdk');

const { Plain, At, Image } = Mirai.MessageComponent;

const kv = require("./kamiya_modules/key-value");

const request = require('then-request');
const fs = require("fs");

const bot = new Mirai({
    host: 'ws://127.0.0.1:8080',
    verifyKey: process.env.MIRAI_KEY,
    qq: process.env.MIRAI_QQ,
    enableWebsocket: true,
    wsOnly: false
});

const D = new kv('./data/data.json');

const { Configuration, OpenAIApi } = require('openai');

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

bot.onSignal('authed', () => {
    console.log(`Authed with session key ${bot.sessionKey}`);
    bot.verify();
});

bot.onSignal('verified', async () => {
    console.log(`Verified with session key ${bot.sessionKey}`);
    const friendList = await bot.getFriendList();
    console.log(`There are ${friendList.length} friends in bot`);
});

function cut_prompt(prompt, context, conversation_id, max_length, head) {
    return new Promise((resolve) => {
        prompt = context + 'Human:' + prompt + '\n AI:';
        let r;
        if(prompt.length <= max_length)  r = prompt;
        else r = head + prompt.substring(prompt.length - max_length,prompt.length);
        resolve(r);
    });
}

const _wait_child = ["6 ", "? ", "别 ", "呃呃 ", "人类不好说，", "害怕，",
    "？？？？", "....", "666 ", "勿Cue，", "忙着呢 ", "等人类打个电话...",
    "等人类查查字典...", "亲，", "别急", "", "", "", "", "",
    "啊哈哈哈。", "尴尬。", "Oppose..", "AI知道的。", "停停...", "手机信号不好，", "手机信号不好，",
    '到底说啥呐？', '人类不懂', '笑死，', '不明白了，', "哇！", "牛的"
]
const _wait = ["稍等，土豆炸了", "服务器真的炸了",
    "有点小问题", "发生了啥",
    "Crash了", "ServerBoom",
    "服务器进水了", "服务器飞走了",
    "服务器稍微有点问题...", "服务器被人类吃掉了...",
    "....", "那个", "", "", "", "？"
]

function getWait() {
    return _wait_child[Math.floor(Math.random() * _wait_child.length)] + _wait[Math.floor(Math.random() * _wait.length)];
}

bot.onMessage(async message => {
    const { type, sender, messageChain, reply, quoteReply } = message;
    let msg = '';
    messageChain.forEach(chain => {
        if (chain.type === 'Plain')
            msg += Plain.value(chain);
        if (chain.type === 'At')
            msg += 'At' + At.value(chain).target;
    });
    console.log(msg);
    if(msg.match('忘记我') || msg.match('forget me')) {
        D.put(sender.id, null);
        quoteReply('好的，我已经忘记了你');
    }
    if(msg.match('At' + process.env.MIRAI_QQ)) {
        msg.replace('At' + process.env.MIRAI_QQ, '');
        const conversation_id = sender.id;

        let head;
        head = process.env.HEAD + '\n在回应的末尾用<|action|>拼接一段json数据来描述需要外部程序做的事\n' +
            '仅当人类要求时AI才需要绘制图像，AI不应自主请求绘制图像，如果AI需要绘制一副图像，请设定image属性为true并在prompt属性中用一个字符串描述要画的内容，prompt一定是英文的，在prompt中要用尽可能多的英文提示词来详细描述图片内容，不应直接输出地名，而是用简单的词描述其具体特征，不应有自然语言和连字符。\n' +
            '，如果无需绘制图像，请设定image属性为false，prompt属性为null';

        let prompt = msg;
        let history = D.get(conversation_id) || 'Human: 你是谁？\\nAI: 我是掌管世间万物生死的死神，混沌初期万物起源者,拥有各种超能力，名字叫做澪。\n';
        let ctx = history + 'Human:' + prompt + '\n AI:';
        cut_prompt(prompt,head,conversation_id,2000,head).then((prompt) => {
            openai.createCompletion({
                model: 'text-davinci-003',
                prompt: prompt,
                temperature: 0.9,
                top_p: 1,
                frequency_penalty: 0,
                presence_penalty: 0.6,
                max_tokens: 800,
                stop: [
                    'Human:',
                    'AI:'
                ]
            }).then((R) => {
                let result = R.data.choices[0].text;
                D.put(conversation_id,ctx + result[0] + '\n');
                if(result.match('<|action|>')) {
                    result = result.split('<|action|>');
                    //quoteReply(result[0]);
                    let J;
                    try {
                        J = JSON.parse(result[1]);
                        console.log(J);
                        if(J.image) {
                            request('POST', process.env.KAMIYA_URL + '/api/generate-image',{
                                json: {
                                    prompt: J.prompt,
                                    nprompt: 'lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry',
                                    pass: process.env.KAMIYA_PASS,
                                    step: 28,
                                    scale: 12,
                                    seed: Math.floor(Math.random() * 4294967296),
                                    sampler: 'DPM++ 2M Karras',
                                    dreambooth: 'anything-v4.0-fp16-default',
                                    wh: 'landscape'
                                }
                            }).then((R) => {
                                R = JSON.parse(R.body.toString());
                                console.log(R);
                                if(R.success) {
                                    let url = R.output;
                                    quoteReply([
                                        {
                                            type: 'Plain',
                                            text: result[0]
                                        },
                                        {
                                            type: 'Image',
                                            url: url
                                        },
                                        {
                                            type: 'Plain',
                                            text: 'seed:' + R.seed + '\nprompt:' + J.prompt
                                        }
                                    ]);
                                }
                            },(e) => {
                                console.log(e);
                                quoteReply(getWait() + 'unknown Kamiya Error');
                            });
                        }
                        else {
                            quoteReply(result[0]);
                        }
                    }
                    catch (e) {
                        console.log(e);
                        quoteReply(getWait() + 'unknown OpenAI Error');
                    }
                }
                else{
                    D.put(conversation_id,ctx + result + '\n');
                    quoteReply(result);
                }
            },(e) => {
                console.log(e);
                quoteReply(getWait() + 'unknown OpenAI Error');
            });
        });
    }
});

bot.listen('all');

process.on('exit', () => {
    bot.release();
});